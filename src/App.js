import React, {Component, Fragment} from 'react';
import './App.css';
import {
  Col
} from 'react-bootstrap';

import TodoList from './components/TodoList/TodoList';
import TodoForm from './components/TodoForm/TodoForm';
import ItemsProvider from './helpers/items-provider'
import TodoSearch from './components/TodoSearch/TodoSearch'

class App extends Component {

  state = {
    todos: [],
    filteredTodos: []
  };

  itemsProvider = new ItemsProvider();

  componentDidMount() {
    this.itemsProvider.getItems()
        .then(todos => this.setState({todos: todos, filteredTodos: todos}))
        .catch(error => error)
  }

  addTodo = (value) => {
    const data = {
      'description': value,
      'done': false
    };
    this.itemsProvider.addItem(data)
        .then((todo) => {
          this.setState(({todos, filteredTodos}) => ({
            todos: [...todos, todo],
            filteredTodos: [...filteredTodos, todo]
          }))
        })
        .catch(error => error)
  };

  deleteTodo = (todoId) => {
    this.itemsProvider.deleteItem(todoId)
        .then(() => this.itemsProvider.getItems())
        .then((todoList) => {
          this.setState(() => ({todos: todoList, filteredTodos: todoList}))
        })
        .catch(error => error)
  };

  toggleTodo = (todoId, isDone) => {
    const data = {"done": isDone};
    this.itemsProvider.updateItem(todoId, data)
        .then(() => this.itemsProvider.getItems())
        .then((todoList) => this.setState(() => ({todos: todoList, filteredTodos: todoList})))
        .catch(error => error)
  };

  editTodo = (todoId, descriptionNew) => {
    const data = {"description": descriptionNew};
    this.itemsProvider.updateItem(todoId, data)
        .then(() => this.itemsProvider.getItems())
        .then((todoList) => this.setState(() => ({todos: todoList, filteredTodos: todoList})))
        .catch(error => error)
  };

  searchTodos = (inputText) => {
    if (inputText === '') {
      this.setState((todos) => ({...todos, filteredTodos: [...this.state.todos]}));
      return;
    }

    const filteredTodos = this.state.filteredTodos.filter(({description}) => description.includes(inputText));

    this.setState((todos) => ({...todos, filteredTodos: [...filteredTodos]}));
  };

  render() {
    return (
        <Fragment>
          <Col md={12}>
            <h3>Add new task</h3>
            <TodoForm submitHandler={this.addTodo}/>
          </Col>
          <Col md={2}>
            <h3>Search the tasks list</h3>
            <TodoSearch searchTodos={this.searchTodos}/>
          </Col>
          <Col md={12}>
            <h3>Tasks</h3>
            <Col md={6}>
              {this.state.filteredTodos.length ?
                  <TodoList todos={this.state.filteredTodos}
                            deleteTodo={this.deleteTodo}
                            toggleTodo={this.toggleTodo}
                            editTodo={this.editTodo}
                  /> :
                  <span>No items found :(</span>
              }


            </Col>
          </Col>
        </Fragment>
    );
  }
}

export default App;
