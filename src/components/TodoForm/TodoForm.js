import React from 'react';
import {
  Button,
  Form,
  FormGroup,
  FormControl
} from 'react-bootstrap';

import './style.css';

export default class TodoForm extends React.Component {

  state = {
    taskDescription: '',
    touched: {
      taskDescription: false
    }
  };

  handleSubmit = (event) => {
    event.preventDefault();
    this.props.submitHandler(this.state.taskDescription);
    this.resetForm();
  };

  handleChange = (event) => {
    this.setState({ taskDescription: event.target.value,
      touched: {...this.state.touched, taskDescription: true} });
  };

  resetForm = () => {
    this.setState({ taskDescription: '',
      touched: {...this.state.touched, taskDescription: false} });
  };

  render() {
    const emptyValue = this.state.taskDescription.length === 0;

    return (
        <Form inline onSubmit={this.handleSubmit}>
          <FormGroup>
            <FormControl
                type="text"
                value={this.state.taskDescription}
                placeholder="Type task text"
                name="taskDescription"
                className={this.state.touched.taskDescription && emptyValue ? "error" : null}
                onChange={this.handleChange}
            />
            <FormControl.Feedback/>
          </FormGroup>
          <Button disabled={emptyValue} type="submit" bsStyle="success">Add</Button>
          {this.state.touched.taskDescription && emptyValue ? <span className="error-text">*Fill in the value please</span> : null}
        </Form>
    )
  }
}