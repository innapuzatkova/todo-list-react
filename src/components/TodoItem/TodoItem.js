import React, {Fragment} from 'react';
import {
  Button,
  ButtonToolbar,
  Checkbox,
  Row,
  Col,
  FormControl
} from 'react-bootstrap';

import "./style.css";

export default class TodoItem extends React.Component {

  state = {
    editMode: false,
    taskDescriptionNew: ''
  };

  handleDeleteClick = () => {
    this.props.deleteTodo(this.props.todo.id);
  };

  toggleTodo = () => {
    this.props.toggleTodo(this.props.todo.id, !this.props.todo.done);
  };

  switchEditMode = () => {
    this.setState((prev) => ({
      editMode: !prev.editMode, taskDescriptionNew: this.props.todo.description
    }))
  };

  handleChange = (event) => {
    const {value} = event.target;
    this.setState((prev) => ({
      ...prev, taskDescriptionNew: value
    }))
  };

  editTodo = () => {
    this.props.editTodo(this.props.todo.id, this.state.taskDescriptionNew)
    this.setState((prev) => ({
      editMode: !prev.editMode
    }))
  };

  render() {
    return (
        <Row className="task-item">
          <Col md={2}>
            <Checkbox onChange={this.toggleTodo} checked={this.props.todo.done}/>
          </Col>
          <Col md={5}>
            {this.state.editMode ?
                <FormControl type="text"
                             value={this.state.taskDescriptionNew}
                             onChange={this.handleChange}/> :
                <span className={this.props.todo.done ? "task-complete" : "task-incomplete"}>
              {this.props.todo.description}
            </span>}
          </Col>
          <Col md={5}>
            <ButtonToolbar className="pull-right">
              <Button bsStyle="primary"
                      bsSize="small"
                      disabled={this.props.todo.done}
                      onClick={this.state.editMode ? this.editTodo : this.switchEditMode}>
                {this.state.editMode ? "Save" : "Edit"}
              </Button>
              <Button bsStyle="danger" bsSize="small" onClick={this.handleDeleteClick}>Delete</Button>
            </ButtonToolbar>
          </Col>
        </Row>
    )
  }
}