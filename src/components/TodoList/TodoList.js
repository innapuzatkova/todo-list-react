import React, {Fragment} from 'react';
import {
  ListGroupItem,
  ListGroup
} from 'react-bootstrap'

import TodoItem from '../TodoItem/TodoItem'

export default class TodoList extends React.Component {

  render() {
    return (
        <ListGroup>
          {this.props.todos.map(item =>
              <ListGroupItem key={item.id}>
                <TodoItem todo={item}
                          toggleTodo={this.props.toggleTodo}
                          deleteTodo={this.props.deleteTodo}
                          editTodo={this.props.editTodo}
                />
              </ListGroupItem>)
          }
        </ListGroup>
    );
  }
}