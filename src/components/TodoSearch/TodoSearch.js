import React from 'react';
import {FormControl} from 'react-bootstrap';

export default class TodoSearch extends React.Component {

  state = {
    text: ''
  };

  handleChange = (event) => {
    const {value} = event.target;
    this.setState({
       text: value
    }, () => {
      this.props.searchTodos(this.state.text);
    });
  };

  render() {
    return(
        <FormControl type="text"
                     value={this.state.text}
                     onChange={this.handleChange}
                     placeholder="Start typing"/>
    )
  }
}