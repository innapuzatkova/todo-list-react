import Request from './request-provider'

export default class ItemsProvider {

  constructor() {
    this.request = new Request();
  }

  getItems() {
    return this.request.getRequest('/todos');
  }

  addItem(data) {
    return this.request.postRequest('/todos', data);
  }

  editItem() {
  }

  deleteItem(id) {
    return this.request.deleteRequest(`/todos/${id}`);
  }

  updateItem(id, data) {
    return this.request.patchRequest(`/todos/${id}`, data);
  }
}