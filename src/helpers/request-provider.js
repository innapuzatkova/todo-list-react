export default class RequestProvider {
  getRequest(url) {
    return fetch(`http://localhost:3000${url}`)
        .then((response) => {
          if (response.ok) {
            return response.json();
          }
        })
        .catch(error => {
          console.log('There has been a problem: ' + error.message);
        });
  }

  postRequest(url, data) {
    return fetch(`http://localhost:3000${url}`, {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
        .then((response) => {
          return response.json();
        })
        .catch(error => {
          console.log('There has been a problem: ' + error.message);
        });
  }

  putRequest() {
  }

  deleteRequest(id) {
    return fetch(`http://localhost:3000${id}`, {
      method: "DELETE",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
        .then((response) => {
          return response.json();
        })
        .catch(error => {
          console.log('There has been a problem: ' + error.message);
        });
  }

  patchRequest(id, data) {
    return fetch(`http://localhost:3000${id}`, {
      method: "PATCH",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
        .then((response) => {
          return response.json();
        })
        .catch(error => {
          console.log('There has been a problem: ' + error.message);
        });
  }
}
